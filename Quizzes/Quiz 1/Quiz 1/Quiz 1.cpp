#include <iostream>
#include <string>

using namespace std;

enum weapon
{
	Dagger,
	Staff,
	Sword
};

struct Stats 
{
	float sta;
	float str;
	float con;
	float atk;
	float def;
};

struct PlayerInfo
{
	string name;
	int level;
	float exp=0;
	weapon weapon;
	int maxHp;
	int curHP;
	int maxMp;
	int currMp;
	
	Stats stat;

	
};



int main()
{
	PlayerInfo pData;
	Stats stats;
	int job = 0;
	int choice = 0;
	cout << "Enter Name of the Character: ";
	cin >> pData.name;
	cout << "Job(1-Mage, 2-Warrior, 3-Rouge): ";
	cin >> job;
	cout << "Level: ";
	cin >> pData.level;
	cout << "STR: ";
	cin >> stats.str;
	cout << "STA: ";
	cin >> stats.sta;
	cout << "CON: ";
	cin >> stats.con;

	system("pause");
	system("cls");
	
	switch (job)
	{
	case 1:
		cout << "Name: " << pData.name << endl;
		cout <<"Job: "<<"Mage" << endl;
		cout << "HP: " << (pData.level * 100) + (5 / stats.sta) << "/" << (pData.level * 100) + (5 / stats.sta) << endl;
		cout << "MP: " << (pData.level * 10) + (3.5 / stats.con) << "/" << (pData.level * 10) + (3.5 / stats.con) << endl;
		cout << "Level: " << pData.level << endl;
		cout << "Exp: " << pData.exp << endl;
		cout << "===================================" << endl;
		cout << "CHARACTER STATS" << endl;
		cout <<"STR: " << stats.str << endl;
		cout <<"STA: " << stats.sta << endl;
		cout << "CON: " << stats.con << endl;
		cout << "ATK: " << (pData.level * 5) + (2 / stats.str) << endl;
		cout << "DEF: " << (pData.level * 2) + (0.6/ stats.sta) << endl;
		cout << "===================================" << endl;
		cout << "CHARACTER EQUIPMENT" << endl;
		cout << "Weapon Name: " << endl;
		cout << "Req Level: " << endl;


		break;
	case 2:
		cout << "Name: " << pData.name << endl;
		cout << "Job: " << "Warrior" << endl;
		cout << "HP: " << (pData.level * 100) + (5 / stats.sta)<<"/"<<(pData.level * 100) + (5 / stats.sta) << endl;
		cout << "MP: " << (pData.level * 10) + (3.5 / stats.con) << "/" << (pData.level * 10) + (3.5 / stats.con) << endl;
		cout << "Level: " << pData.level << endl;
		cout << "Exp: " << pData.exp << endl;
		cout << "===================================" << endl;
		cout << "CHARACTER STATS" << endl;
		cout << "STR: " << stats.str << endl;
		cout << "STA: " << stats.sta << endl;
		cout << "CON: " << stats.con << endl;
		cout << "ATK: " << (pData.level * 5) + (2 / stats.str)  << endl;
		cout << "DEF: " << (pData.level * 2) + (0.6 / stats.sta) << endl;
		cout << "===================================" << endl;
		cout << "CHARACTER EQUIPMENT" << endl;
		cout << "Weapon Name: " << endl;
		cout << "Req Level: " << endl;
		break;
	case 3:
		cout << "Name: " << pData.name << endl;
		cout << "Job: " << "Rogue" << endl;
		cout << "HP: " << (pData.level * 100) + (5 / stats.sta) << "/" << (pData.level * 100) + (5 / stats.sta) << endl;
		cout << "MP: " << (pData.level * 10) + (3.5 / stats.con) << "/" << (pData.level * 10) + (3.5 / stats.con) << endl;
		cout << "Level: " << pData.level << endl;
		cout << "Exp: " << pData.exp << endl;
		cout << "===================================" << endl;
		cout << "CHARACTER STATS" << endl;
		cout << "STR: " << stats.str << endl;
		cout << "STA: " << stats.sta << endl;
		cout << "CON: " << stats.con << endl;
		cout << "ATK: " << (pData.level * 5) + (2 / stats.str) << endl;
		cout << "DEF: " << (pData.level * 2) + (0.6 / stats.sta) << endl;
		cout << "===================================" << endl;
		cout << "CHARACTER EQUIPMENT" << endl;
		cout << "Weapon Name: " << endl;
		cout << "Req Level: " << endl;
		break;
	}
	
	cout << "         " << endl;
	cout << "         " << endl;
	cout << "         " << endl;
	cout << "1-Equip Weapon " << endl;
	cout << "2-Modify Level " << endl;
	cout << "3-Modify Stats " << endl;
	cout << "x-Exit the game " << endl;
	cout << "         " << endl;
	cout << "Enter Action: " << endl;
	cin >> choice;

	switch (choice)
	{
	case 1:
		break;
	case 2:
		cout << "Level: ";
		cin >> pData.level;
		break;
	case 3:
		cout << "STR: ";
		cin >> stats.str;
		cout << "STA: ";
		cin >> stats.sta;
		cout << "CON: ";
		cin >> stats.con;
	}


	system("pause");
	return 0;

}