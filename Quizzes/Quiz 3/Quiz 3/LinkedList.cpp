#include "LinkedList.h"



LinkedList::LinkedList()
{
	head = nullptr;
	tail = nullptr;
}


LinkedList::~LinkedList()
{
}

void LinkedList::Add(int n)
{
	Node *temp = new Node;
	temp->data = n;
	temp->next = nullptr;

	if (head == nullptr)
	{
		head = temp;
		tail = temp;
	}
	else
	{
		tail->next = temp;
		tail = tail->next;
	}
}

void LinkedList::Print()
{
	Node * current;

	current = head;

	while (current != nullptr)
	{
		cout << current->data << endl;
		current = current->next;
	}

}

void LinkedList::Push(int val)
{
	Node* temp = new Node;
	temp->data = val;
	temp->next = nullptr;

	if (head != nullptr)
	{
		temp->next = head;
	}
	head = temp;
}

void LinkedList::Insert(int data, int n)
{
	Node* temp1 = new Node;
	temp1->data = data;
	temp1->next = nullptr;
	if (n == 1)
	{
		temp1->next = head;
		head = temp1;
		return;
	}

	Node* temp2 = head;
	for (int i = 0; i < n - 2; i++)
	{
		temp2 = temp2->next;
	}
	temp1->next = temp2->next;
	temp2->next = temp1;
}

void LinkedList::Delete(int n)
{
	Node* temp1 = head;
	if (n == 1)
	{
		head = temp1->next;
		free(temp1);
		return;
	}
	int i;
	for (int i = 0; i < n - 2; i++)
	{
		temp1 = temp1->next;
	}
	Node* temp2 = temp1->next;
	temp1->next = temp2->next;
	free(temp2);
}

void LinkedList::Reverse()
{
	Node* current;
	Node* prev;
	Node* next;

	current = head;
	prev = nullptr;

	while (current != nullptr)
	{
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	head = prev;
}

void LinkedList::ReversePrint(Node* prev)
{
	if (prev == nullptr)
	{
		return;
	}
	ReversePrint(prev->next);
	cout << prev->data << endl;
}

void LinkedList::Pop()
{
	Node* current;
	Node* prev;
	Node* next;

	current = tail;
	prev = nullptr;
	if (tail != nullptr)
	{
		free(tail);
	}

	next = current->next;
	current->next = prev;
	
	prev=tail;
	

}

void LinkedList::Swap(int value,int value2)
{
	Node*temp1;
	Node*temp2;
	//sir kahit 1 lang po 
	

}

void LinkedList::BubbleSort()
{
	Node*pnt;
	Node*s;
	int value;
	if (head == NULL) {
		cout << "EMPTY LIST" << endl;
	}
	pnt = head;
	while (pnt != NULL)
	{
		for (s = pnt->next;s!=NULL; s = s->next)
		{
			if (pnt->data > s->data)
			{
				value = pnt->data;
				pnt->data = s->data;
				s->data = value;
			}
			pnt = pnt->next;
		}
	}
}

void LinkedList::SelectionSort()
{

	Node*pnt, *s;
	int value;
	if (head == NULL) {
		cout << "EMPTY LIST" << endl;
	}
	pnt = head;
	while (pnt != NULL)
	{
		for (s = pnt->next; s!=NULL; s = s->next)
		{
			if (pnt->data < s->data)
			{
				value = pnt->data;
				pnt->data = s->data;
				s->data = value;
			}
			pnt = pnt->next;
		}
	}
}
	
