#include <iostream>
#include <string>
#include "LinkedList.h"

using namespace std;

int main() 
{
	cout << "=========CURRENT LINKED LIST===============" << endl;
	LinkedList list;
	list.Add(100);
	list.Add(20);
	list.Add(301);
	list.Add(4550);
	list.Add(5089);
	list.Print();
	cout << "============================================" << endl;
	cout << "                 " << endl;
	int choice;
	

	cout << "1 - Push" << endl;
	cout << "2 - Pop" << endl;
	cout << "3 - Insert" << endl;
	cout << "4 - Delete" << endl;
	cout << "5 - Swap" << endl;
	cout << "6 - Selection Sort" << endl;
	cout << "7 - Bubble Sort" << endl;

	cout << "What do you want to do? " << endl;
	cin >> choice;

	switch (choice)
	{
	case 1 :
		int value;
		cout << "ENTER A VALUE: ";
		cin >> value;
		
		list.Push(value);

		cout << "=========NEW LINKED LIST===============" << endl;
		list.Print();
		cout << "============================================" << endl;

		break;

	case 2:
		list.Pop();
		cout << "=========NEW LINKED LIST===============" << endl;
		list.Print();
		cout << "============================================" << endl;

		break;
	case 3:
		int data;
		int pos;
		cout << "ENTER A VALUE: ";
		cin >> data;
		cout << "ENTER DESIRED INDEX: ";
		cin >> pos;

		list.Insert(data, pos);

		cout << "=========NEW LINKED LIST===============" << endl;
		list.Print();
		cout << "============================================" << endl;

		break;
	case 4:
		int dex;
		
		cout << "WHAT WOULD LIKE TO DELETE: ";
		cin >> dex;

		list.Delete(dex);

		cout << "=========NEW LINKED LIST===============" << endl;
		list.Print();
		cout << "============================================" << endl;

		break;
	case 5:
		
		break;
	case 6:
		list.BubbleSort();
		cout << "=========NEW LINKED LIST===============" << endl;
		list.Print();
		cout << "============================================" << endl;
		break;

		break;
	case 7:
		list.SelectionSort();
		cout << "=========NEW LINKED LIST===============" << endl;
		list.Print();
		cout << "============================================" << endl;
		break;

		break;
	}















	system("pause");
	return 0;
};