#pragma once
#pragma once
#include <iostream>

using namespace std;

struct Node
{
	int data;
	Node *next;
};

class LinkedList
{
public:
	LinkedList();
	~LinkedList();

	Node *head;
	Node *tail;

	void Add(int n);
	void Print();
	void Push(int val);
	void Insert(int data, int n);
	void Delete(int n);
	void Reverse();
	void ReversePrint(Node* prev);
	void Pop();
	void Swap(int value, int value2);
	void BubbleSort();
	void SelectionSort();

};


