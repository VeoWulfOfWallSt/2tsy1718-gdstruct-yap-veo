#include "LinkedList.h"
#include<iostream>
using namespace std;
struct Node
{
	int data;
	Node* next;
};

Node*header = nullptr;
Node*tail = nullptr;

LinkedList::LinkedList()
{
	header = nullptr;
	tail = nullptr;
}

LinkedList::~LinkedList()
{
	Node *nextNode = header;
	while (nextNode != nullptr)
	{
		Node *deleteNode = nextNode;
		nextNode = nextNode->next;     // save pointer to the next element
		delete deleteNode;       // delete the current entry
	}
}
void LinkedList::Add(int val)
{
	Node* temp = new Node();
	temp->data = val;
	temp->next = nullptr;

	if (header == nullptr)
	{
		header = temp;
		tail = temp;
	}
	else
	{
		tail->next = temp;
		tail = tail->next;
	}
}

void LinkedList::Print()
{
	Node* current;
	current = header;

	while (current != nullptr)
	{
		std::cout << current->data << std::endl;
		current = current->next;
	}
}
void LinkedList::reverse() {
	if (header != NULL && header->next != NULL) {
		Node* x = header;
		Node* y = header->next;
		Node* z = NULL;
		if (y->next != NULL) 
			z = y->next;
		header->next = NULL;
		while (z->next != NULL) {
			y->next = x;
			x = y;
			y = z;
			z = z->next;
		}
		y->next = x;
		header = z;
		z->next = y;
	}

};
void LinkedList::push(int val)
{
	Node* temp = new Node();
	temp->data = val;
	temp->next = header;
	header = temp;
}
void LinkedList::insert(struct Node* previous, int new_data)
{
	
	if (previous == NULL)
	{
		printf("ERROR");
		return;
	}

	
	struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));

	
	new_node->data = new_data;

	
	new_node->next = previous->next;

	
	previous->next = new_node;
}
void LinkedList::del(Node * node)
{
	Node * temp = node->next;
	node->data = node->next->data;
	node->next = temp->next;
	free(temp);
}



int main()
{
	
	
	LinkedList testlist;
	testlist.Add(10);
	testlist.Add(20);
	testlist.Add(30);
	testlist.Add(40);
	testlist.Add(50);
	testlist.push(60);
	testlist.Print();
	testlist.insert(header->next,9);
	testlist.del(Node * 10);
	testlist.Print();

	
	
	system("pause");
	return 0;
}