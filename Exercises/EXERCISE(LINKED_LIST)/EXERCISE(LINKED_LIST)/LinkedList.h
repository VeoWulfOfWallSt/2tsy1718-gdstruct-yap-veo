#pragma once
#include <iostream>
using namespace std;

class LinkedList
{
public:
	LinkedList();
	~LinkedList();
	struct Node
	{
		int data;
		Node* next;
	};

	Node*header = nullptr;
	Node*tail = nullptr;
	void Add(int val);
	void Print();
	void reverse();
	void push(int val);
	void insert(struct Node* previous, int new_data);
	void del(Node * node);
	
};