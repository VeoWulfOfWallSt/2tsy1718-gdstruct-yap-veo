#pragma once
class Character
{
public:
	Character();
private:
	string name;
	float health;
	float armor;
	enum equipment;
	string getName();
	float getHealth();
	float getCurrHp();
	float getArmor();
	float getCurrarmor();
	void getEquipment();
	bool useEquipment();
	bool dropEquipment();
	bool isAlive();

	~Character();
};

