
#include <iostream>

using namespace std;

int main()
{

	cout << "array values :" << "9, 10, 2, 3, 50 ,90,50,20,35,40" << "\n";
	
	int array[10] = { 9,10,2,3,50,90,50,20,35,40 };

	cout << "sorted values : ";

	
	for (int startIndex = 0; startIndex < 10; ++startIndex)
	{
		
		int smallestIndex = startIndex;

		
		for (int nowIndex = startIndex + 1; nowIndex < 10; ++nowIndex)
		{
			
			if (array[nowIndex] < array[smallestIndex])
				
			{
				smallestIndex = nowIndex;
			}
		}

		
		swap(array[startIndex], array[smallestIndex]);
	}

	
	for (int index = 0; index < 10; ++index)
	{
		cout << array[index] << ' ';
	}

	cout << "\n";
	system("pause");
	return 0;
}

