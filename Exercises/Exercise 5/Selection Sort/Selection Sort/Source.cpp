#include <iostream>

using namespace std;

int main()
{
	int myNumbers[10], i, n, p, k, min, loc, temp;


	cout << "Enter No. of Elements:";
	cin >> n;

	cout << "Enter Elements:";
	for (i = 1; i <= n; i++)
	{
		cin >> myNumbers[i];
	}

	for (p = 1; p <= n - 1; p++)
	{
		min = myNumbers[p];
		loc = p;

		for (k = p + 1; k <= n; k++)
		{
			if (min > myNumbers[k])
			{
				min = myNumbers[k];
				loc = k;
			}
		}
		temp = myNumbers[p];
		myNumbers[p] = myNumbers[loc];
		myNumbers[loc] = temp;
	}

	cout << "\nAfter Sorting : \n";

	for (i = 1; i <= n; i++)
	{
		cout << myNumbers[i] << ",";

	}
	system("pause");
	return 0;
}