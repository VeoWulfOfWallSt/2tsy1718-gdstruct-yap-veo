#include <iostream>
#include <string>

using namespace std;

int getHP(int lvl, int STA);
int getMP(int lvl, int SPR);
int getAtk(int lvl, int STR);
int main()
{

	string name;
	int lvl;
	int STR;
	int STA;
	int SPR;

	int HP;
	int MP;
	int Atk;

	cout << "Input Name: ";
	cin >> name;

	cout << "Input Level: ";
	cin >> lvl;

	cout << "Input STR: ";
	cin >> STR;

	cout << "Input STA: ";
	cin >> STA;

	cout << "Input SPR: ";
	cin >> SPR;


	HP = getHP(lvl, STA);
	MP = getMP(lvl, SPR);
	Atk = getAtk(lvl, STR);

	cout << "HP: " << HP << endl;
	cout << "MP: " << MP << endl;;
	cout << "Atk: " << Atk << endl;;
	system("pause");
	return 0;

};

int getHP(int lvl, int STA)
{
	int maxHp;
	int sHp;
	sHp = ((STA / 100)*(lvl * 100));
	maxHp = ((lvl * 100) +sHp);
	return maxHp;

}

int getMP(int lvl, int SPR)
{
	int maxMP;
	int sMp;
	sMp = ((SPR / 100)*(lvl * 10));
	maxMP= ((lvl * 10) + sMp);
	return maxMP;
}

int getAtk(int lvl, int STR)
{
	int maxAtk;
	int sAtk;
	sAtk = ((STR / 100)*(lvl * 2));
	maxAtk = ((lvl * 2) + sAtk);

	return maxAtk;
}
