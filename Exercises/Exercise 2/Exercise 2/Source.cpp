#include <iostream>
#include <string>

using namespace std;

enum ClassType
{
	Warrior,
	Archer,
	Sorceress,
	Cleric,
	Academic,
	Kali,
	Assassin,
	Lancea,
	Machina,
};

struct Stat
{
	int STR;
	int AGI;
	int INT;
	int VIT;
	int WIL;
};

struct PlayerData
{
	string name;
	ClassType classType;
	int baseLevel = 1;

	int curHP;
	int maxHP;
	int curSP;
	int maxSP;

	Stat stat;

	int PDMG;
	int MDMG;
	int PDEF;
	int MDEF;




	float getMaxHP()
	{
		int vitHP;
		vitHP = maxHP * (stat.VIT * 0.25);
		maxHP = maxHP + vitHP;
		return maxHP;
	};

	int getMaxSP()
	{
		int wilSP;
		wilSP = maxSP * (stat.WIL * 0.25);
		maxSP = maxSP + wilSP;
		return maxSP;
	}

	int getPATK()
	{
		int strATK;
		strATK = PDMG * (stat.STR * 0.5);
		PDMG = PDMG + strATK;
		return PDMG;
	}

	int getMATK()
	{
		int intMATK;
		intMATK = MDMG * (stat.INT * 0.5);
		MDMG = MDMG + intMATK;
		return MDMG;
	}

	int getPDEF()
	{
		int vitDEF, agiDEF;
		vitDEF = stat.VIT / 2;
		agiDEF = stat.AGI / 5;
		PDEF = PDEF + vitDEF + agiDEF;
		return PDEF;
	}

	int getMDEF()
	{
		int wilMDEF, agiDEF;
		wilMDEF = stat.WIL / 2;
		agiDEF = stat.AGI / 5;
		MDEF = MDEF + wilMDEF + agiDEF;
		return MDEF;
	}

	string Guild;

};

struct Equipment
{
	string helmet;
	string chestArmor;
	string pants;
	string gloves;
	string boots;
	string weapon1;
	string weapon2;
};

struct Accessories
{
	string necklace;
	string earRing;
	string ring1;
	string ring2;
	string item;
};

PlayerData AddToCharacterList()
{
	PlayerData pData;
	cout << "Enter Name of the Character: ";
	cin >> pData.name;
	cout << endl;
	cout << "Enter Guild Name: ";
	cin >> pData.Guild;
	cout << endl;
	return pData;
}

int main()
{
	PlayerData pData[5];
	cout << "Character List" << endl;
	for (int i = 0; i < 5; i++)
	{
		pData[i] = AddToCharacterList();
	}

	for (int i = 0; i < 5; i++)
	{
		ClassType type;
		int chrClass;
		cout << "Name: " << i + 1 << " : " << pData[i].name << endl;
		cout << endl;
		cout << "Choose a Class :[1] Warrior ,[2] Archer ,[3] Cleric ,[4] Sorceress ,[5] Academic ,[6] Kali ,[7] Assassin ,[8] Lancea ,[9] Machina" << endl;
		cin >> chrClass;
		pData[i].maxHP = 100;
		pData[i].maxSP = 100;
		pData[i].PDMG = 10;
		pData[i].MDMG = 10;
		pData[i].PDEF = 10;
		pData[i].MDEF = 10;
		switch (chrClass)
		{
		case 1:
			type = Warrior;
			pData[i].stat.STR = 4;
			pData[i].stat.AGI = 2;
			pData[i].stat.INT = 1;
			pData[i].stat.VIT = 6;
			pData[i].stat.WIL = 2;
			break;
		case 2:
			type = Archer;
			pData[i].stat.STR = 5;
			pData[i].stat.AGI = 5;
			pData[i].stat.INT = 2;
			pData[i].stat.VIT = 1;
			pData[i].stat.WIL = 2;
			break;
		case 3:
			type = Cleric;
			pData[i].stat.STR = 2;
			pData[i].stat.AGI = 2;
			pData[i].stat.INT = 3;
			pData[i].stat.VIT = 3;
			pData[i].stat.WIL = 5;
			break;
		case 4:
			type = Sorceress;
			pData[i].stat.STR = 1;
			pData[i].stat.AGI = 2;
			pData[i].stat.INT = 5;
			pData[i].stat.VIT = 2;
			pData[i].stat.WIL = 4;
			break;
		case 5:
			type = Academic;
			pData[i].stat.STR = 1;
			pData[i].stat.AGI = 1;
			pData[i].stat.INT = 5;
			pData[i].stat.VIT = 3;
			pData[i].stat.WIL = 3;
			break;
		case 6:
			type = Kali;
			pData[i].stat.STR = 2;
			pData[i].stat.AGI = 5;
			pData[i].stat.INT = 4;
			pData[i].stat.VIT = 1;
			pData[i].stat.WIL = 3;
			break;
		case 7:
			type = Assassin;
			pData[i].stat.STR = 4;
			pData[i].stat.AGI = 5;
			pData[i].stat.INT = 2;
			pData[i].stat.VIT = 3;
			pData[i].stat.WIL = 1;
			break;
		case 8:
			type = Lancea;
			pData[i].stat.STR = 6;
			pData[i].stat.AGI = 4;
			pData[i].stat.INT = 1;
			pData[i].stat.VIT = 2;
			pData[i].stat.WIL = 2;
			break;
		case 9:
			type = Machina;
			pData[i].stat.STR = 3;
			pData[i].stat.AGI = 1;
			pData[i].stat.INT = 4;
			pData[i].stat.VIT = 1;
			pData[i].stat.WIL = 6;
			break;
		}
		cout << "HP: " << pData[i].getMaxHP() << endl;
		cout << "SP: " << pData[i].getMaxSP() << endl;
		cout << "P.Attack: " << pData[i].getPATK() << endl;
		cout << "M.Attack: " << pData[i].getMATK() << endl;
		cout << "P.Defense: " << pData[i].getPDEF() << endl;
		cout << "M.Defense: " << pData[i].getMDEF() << endl;
		cout << endl;
	}
	system("pause");
	return 0;

}