#pragma once
#include <iostream>
#include "Inventory.h"
#include <string>
class Player
{
public:
	Player();
	~Player();
	int itemNum;
	int choice;
	int hp();
	int maxHp();
	int lvl();
	int atk();
	int def();
	int getExp();
	int pow();
	void displayHp();
	void pokemon();
	void addStats();
	void battleDmg(int dmg);
	void finalDisp();
	int getDmg(int level, int pow, int atk, int def);
	int reward(int exp, int lvl, int pow);

private:
	int mHp;
	int mMaxhp;
	int mLvl;
	int mAtk;
	int mDef;
	int mPow;
	int mExp;
};
