#include "Pokemon.h"



Pokemon::Pokemon()
{

}
string Pokemon::getName()
{
	return mName;
}

int Pokemon::hp()
{
	return mHp;
}

int Pokemon::maxHp()
{
	return mMaxhp;
}

int Pokemon::lvl()
{
	return mLvl;
}

int Pokemon::atk()
{
	return mAtk;
}

int Pokemon::def()
{
	return mDef;
}

int Pokemon::getExp()
{
	return 0;
}
int Pokemon::pow()
{
	return mPow;
}




void Pokemon::enemyPokemon(int rngNumber)
{
	switch (rngNumber)
	{
	case 1:
		mName = "Caterpie";
		mLvl = 1;
		mHp = 45;
		mAtk = 30;
		mDef = 35;
		mPow = 10;
		mExp = 300;
	case 2:
		mName = "Weedle";
		mLvl = 1;
		mHp = 40;
		mAtk = 35;
		mDef = 30;
		mPow = 10;
		mExp = 350;
	case 3:
		mName = "Pidgey";
		mLvl = 1;
		mHp = 40;
		mAtk = 45;
		mDef = 40;
		mPow = 10;
		mExp = 359;
	case 4:
		mName = "Rattata";
		mLvl = 1;
		mHp = 30;
		mAtk = 56;
		mDef = 35;
		mPow = 10;
		mExp = 367;
	case 5:
		mName = "Oddish";
		mLvl = 1;
		mHp = 45;
		mAtk = 50;
		mDef = 55;
		mPow = 20;
		mExp = 500;
	case 6:
		mName = "Venonat";
		mLvl = 1;
		mHp = 60;
		mAtk = 55;
		mDef = 50;
		mPow = 10;
		mExp = 1000;
	case 7:
		mName = "Zubat";
		mLvl = 1;
		mHp = 40;
		mAtk = 45;
		mDef = 35;
		mPow = 10;
		mExp = 750;
	default:
		break;
	}
}

void Pokemon::displayHp()
{
	cout << "HP : " << mHp << "/" << mMaxhp << endl;
}
void Pokemon::stats()
{
	int atk, def, Hp = 5;
	atk = rand() % (3) + 2;
	def = rand() % (3) + 2;
	mAtk = mAtk + atk;
	mDef = mDef + def;
	mDef = mDef + def;
	mHp = mHp + Hp;
	mMaxhp = mMaxhp + Hp;
}
int Pokemon::getDmg(int level, int pow, int atk, int def)
{
	float dmg, x;
	dmg = ((((((2 * level) / 5) + 2)*pow*(atk / def)) / 50) + 2);
	x = rand() % 10 + 1;
	if (x >= 2)
	{
		dmg = dmg + (dmg*0.75);
	}
	return dmg;
}


void Pokemon::battleDmg(int dmg)
{
	mHp = mHp - dmg;

}



Pokemon::~Pokemon()
{
}
