#include "Player.h"
#include <string>


Player::Player()
{
	
}

int Player::hp()
{
	return mHp;
}




int Player::maxHp()
{
	return mMaxhp;
}
int Player::pow() 
{
	return mPow;
}

int Player::lvl()
{
	return mLvl;
}

int Player::atk()
{
	return mAtk;
}

int Player::def()
{
	return mDef;
}

int Player::getExp()
{
	return mExp;
}

void Player::displayHp()
{
	cout << "HP : " << mHp << "/" << mMaxhp << endl;
}

void Player::pokemon()
{
	cout << "CHOOSE YOUR STARTER POKEMON:" << endl;
	cout << " 1 - Bulbasuar " << endl;
	cout << " 2 - Charmander " << endl;
	cout << " 3 - Squirtle " << endl;

	cin >> choice;
	cout << "==========================================" << endl;
	switch (choice)
	{
	case 1:
		cout << "YOU CHOOSE BULBASAUR" << endl;
		mHp = 45;
		mMaxhp = 45;
		mLvl = 1;
		mAtk = 49;
		mDef = 49;
		mPow = 20;

		cout << "HP : " << mHp << endl;
		cout << "Level : " << mLvl << endl;
		cout << "Attack : " << mAtk << endl;
		cout << "Defense : " << mDef << endl;
		cout << "Power : " << mPow << endl;

		break;
	case 2:
		cout << "YOU CHOOSE CHARMANDER" << endl;
		mHp = 39;
		mMaxhp = 39;
		mLvl = 1;
		mAtk = 52;
		mDef = 43;
		mPow = 25;

		cout << "HP : " << mHp << endl;
		cout << "Level : " << mLvl << endl;
		cout << "Attack : " << mAtk << endl;
		cout << "Defense : " << mDef << endl;
		cout << "Power : " << mPow << endl;
		break;
	case 3:
		cout << "YOU CHOOSE SQUIRTLE" << endl;
		mHp = 44;
		mMaxhp = 44;
		mLvl = 1;
		mAtk = 48;
		mDef = 65;
		mPow = 15;

		cout << "HP : " << mHp << endl;
		cout << "Level : " << mLvl << endl;
		cout << "Attack : " << mAtk << endl;
		cout << "Defense : " << mDef << endl;
		cout << "Power : " << mPow << endl;
		break;

	}

	system("pause");

}

void Player::addStats()
{
	int atk, def, Hp = 5;
	atk = rand() % (3) + 2;
	def = rand() % (3) + 2;
	mAtk = mAtk + atk;
	mDef = mDef + def;
	mDef = mDef + def;
	mHp = mHp + Hp;
	mMaxhp = mMaxhp + Hp;
}

int Player::getDmg(int level,int pow,int atk,int def)
{
	float dmg, x;
	dmg = ((((((2 * level) / 5) + 2)*pow*(atk / def)) / 50) + 2);
	x = rand() % 10 + 1;
	if (x >= 2)
	{
		dmg = dmg + (dmg*0.75);
	}
	return dmg;
}

int Player::reward(int exp,int lvl,int pow)
{
	int rewardExp;
	rewardExp = ((exp*lvl*pow) / 7);
	mExp = mExp + exp;
	if (mExp >= 20000000)
	{
		mLvl = 40;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 15000000)
	{
		mLvl = 39;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 12000000)
	{
		mLvl = 38;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 9500000)
	{
		mLvl = 37;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 7500000)
	{
		mLvl = 36;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 6000000)
	{
		mLvl = 35;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 4750000)
	{
		mLvl = 34;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 3750000)
	{
		mLvl = 33;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 3000000)
	{
		mLvl = 32;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 2500000)
	{
		mLvl = 31;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 2000000)
	{
		mLvl = 30;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 1650000)
	{
		mLvl = 29;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 1350000)
	{
		mLvl = 28;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 1100000)
	{
		mLvl = 27;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 900000)
	{
		mLvl = 26;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 710000)
	{
		mLvl = 25;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 560000)
	{
		mLvl = 24;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 435000)
	{
		mLvl = 23;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 335000)
	{
		mLvl = 22;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 260000)
	{
		mLvl = 21;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 210000)
	{
		mLvl = 20;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 185000)
	{
		mLvl = 19;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 160000)
	{
		mLvl = 18;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 140000)
	{
		mLvl = 17;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 120000)
	{
		mLvl = 16;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 100000)
	{
		mLvl = 15;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 85000)
	{
		mLvl = 14;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 75000)
	{
		mLvl = 13;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 65000)
	{
		mLvl = 12;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 55000)
	{
		mLvl = 11;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 45000)
	{
		mLvl = 10;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 36000)
	{
		mLvl = 9;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 28000)
	{
		mLvl = 8;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 21000)
	{
		mLvl = 7;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 15000)
	{
		mLvl = 6;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 10000)
	{
		mLvl = 5;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 6000)
	{
		mLvl = 4;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 3000)
	{
		mLvl = 3;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 1000)
	{
		mLvl = 2;
		cout << "YOU LEVELED UP" << endl;
		addStats();
	}
	else if (mExp >= 0)
	{
		mLvl = 1;
	}
	return exp;
}

Player::~Player()
{
}
void Player::battleDmg(int dmg)
{
	mHp = mHp - dmg;

}

void Player::finalDisp()
{
	cout << "HP :" << mHp << endl;
	cout << "Level :" << mLvl << endl;
	cout << "DEF :" << mDef << endl;
	cout << "ATK :" << mAtk << endl;
	cout << "POW :" << mPow << endl;
	cout << "EXP :" << mExp << endl;
}



