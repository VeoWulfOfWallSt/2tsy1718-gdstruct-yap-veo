#pragma once
#include <iostream>
#include "Inventory.h"
#include "Player.h"

using namespace std;
class Pokemon
{
public:
	Pokemon();
	~Pokemon();
	int choice;
	string getName();
	int hp();
	int maxHp();
	int lvl();
	int atk();
	int def();
	int pow();
	int getExp();
	int getDmg(int level, int pow, int atk, int def);
	void battleDmg(int dmg);
	void stats();
	void displayHp();
	void enemyPokemon(int rngNumber);
private :
	string mName;
	int mHp;
	int mMaxhp;
	int mLvl;
	int mAtk;
	int mDef;
	int mOriginaldef;
	int mPow;
	int mExp;
};

